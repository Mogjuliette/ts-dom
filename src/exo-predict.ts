/* Texte Prédictif nul
1. Dans un fichier exo-predict.html créer un paragraphe et 5 button tous avec une class "prediction" et qui contiendront "hello" "bye" "yes" "no" "please"
2. Côté typescript, sélectionner tous ces button avec un querySelectorAll
3. Faire une boucle sur ces buttons, et dans la boucle, faire un addEventListener sur chaque button qui fera pour le moment juste un console.log de n'importe quoi
4. Modifier le console.log pour faire que plutôt que n'importe quoi, il affiche le text du bouton qui a été cliqué
5. Concaténer le texte en question dans le paragraphe
6. Rajouter un input type text dans le html et le récupérer dans le typescript
7. Faire un eventListener sur l'event "input" (qui se déclenche à chaque fois que la valeur de l'input change) qui va prendre la valeur actuelle de l'input puis faire une boucle sur les buttons et faire en sorte de n'afficher que les bouton dont le text contient la valeur de l'input (il y a une fonction text.includes('blup') qui renvoie true ou false si la variable text contient 'blup') 
 */
let predict = document.querySelectorAll<HTMLElement>('.prediction')
let paragraphe = document.querySelector<HTMLParagraphElement>('p')
let text = document.querySelector<HTMLInputElement>('#text')
let textValue ="rien"


for (const prediction of predict) {
    prediction.addEventListener('click',()=>{
        console.log(prediction.innerHTML)
        if (paragraphe){
            paragraphe.innerHTML += prediction.innerHTML+" "
        }
    })
}


text?.addEventListener("keyup",()=>{
    if(text){
       textValue = text.value  
       for (const prediction of predict) {
        if(!prediction.innerHTML.toLowerCase().includes(textValue.toLowerCase())){
            prediction.style.display="none"
        } else {
            prediction.style.display="inline"
        }
       }
    }
})