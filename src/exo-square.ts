/* The amazing moving square
1. Créer un nouveau fichier html/ts pour exo-square
2. Dans le html, créer une div#playground et lui mettre en css  une height à 100vh, et une position relative. Dans cette div, rajouter une autre div#square et faire en sorte via le css qu'elle ressemble à un carré rouge (ou autre chose, on s'en fout un peu) et qu'elle soit en position absolute

3. Dans le TS, capturer le playground et le square puis rajouter un addEventListener au click sur le playground
4. Dans ce listener, rajouter un argument event à la fat-arrow, regarder via le console log si le event ne contiendrait pas des valeurs indiquant où on a cliqué (spoiler : c'est le cas)
5. Utiliser ces valeurs pour les assigner en top et en left de notre square (il va falloir concaténé 'px' aux valeurs en question)
6. On peut rajouter une petite transition à notre css sur le carré pour qu'il glisse tel un cygne sur un lac gelé  */

let playground = document.querySelector<HTMLDivElement>("#playground")
let square = document.querySelector<HTMLDivElement>("#square")
let square2 = document.querySelector<HTMLDivElement>("#square2")
let square3 = document.querySelector<HTMLDivElement>("#square3")


playground?.addEventListener('click', (event) => {
     if (square){
        square.style.left = event.clientX+"px";
        square.style.top = event.clientY+"px";
     }  
})


playground?.addEventListener('drag',(event)=>{
    if (square2){
        square2.style.left = event.clientX+"px"
        square2.style.top = event.clientY+"px"
     }
})

playground?.addEventListener('mousemove',(event)=>{
    if (square3){
        square3.style.left = event.clientX+"px"
        square3.style.top = event.clientY+"px"
     }
})




