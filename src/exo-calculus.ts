/* nouveaux fichier exo-dom-function html/ts
1. Dans le fichier HTML, rajouter un form qui contiendra 2 input type number ainsi qu'un span qui contiendra le résultat du calcul, ainsi qu'un button = qui lancera le calcul
2. Côté typescript, faire en sorte de rajouter un eventListener sur le submit du formulaire qui va récupérer la propriété value de chaque input, et les afficher en console
3. Pour que le formulaire ne recharge pas la page, il va falloir rajouter l'argument event et dans la fonction commencer par déclencher un event.preventDefault()
4. Une fois qu'on a les valeurs dans la console, faire en sorte de les additionner et afficher le résultat en console
5. Quand ça marche, faire en sorte d'assigner ce résultat au span
6. Rajouter dans le html un select qui aura comme options + - / et *, modifier l'eventListener pour faire qu'on récupère la value de ce select et selon l'opérateur on fait un calcul ou un autre
7. Externaliser la logique du calcul dans une fonction qui attendra 3 arguments, les deux nombres et l'opérateur */

const form = document.querySelector<HTMLFormElement>('form');
const nombre1 = document.querySelector<HTMLInputElement>('#nombre1');
const nombre2 = document.querySelector<HTMLInputElement>('#nombre2');
let span = document.querySelector<HTMLSpanElement>('span');
let operateur = document.querySelector<HTMLSelectElement>('#operateur')
let a = 0;
let b = 0;
let result = 0;
let operateurValue = ""


form?.addEventListener('submit',(event) => {
    event.preventDefault();
    if (nombre1 != null && nombre2 != null && operateur != null) {
        a = nombre1?.valueAsNumber;
        b = nombre2?.valueAsNumber;
        operateurValue = operateur.value;
        calcul(a,b,operateurValue);
        if (span) {
                span.innerHTML = String(result);
            }
    }
})


function calcul(a:number,b:number,c:string):number {
        if (c == "+") {
            result = a + b;
            console.log("Le résultat de "+a+c+b+" est "+result+"");            
            return result;
        }
        if (c == "-") {
            let result = a - b;
            console.log("Le résultat de "+a+c+b+" est "+result+"");
            return result;
        }
        if (c == "/") {
            let result = a / b;
            console.log("Le résultat de "+a+c+b+" est "+result+"");
            return result;
        }
        if (c == "*") {
            let result = a * b;
            console.log("Le résultat de "+a+c+b+" est "+result+"");
            return result;
        }
        throw console.error("non valide");
    }
