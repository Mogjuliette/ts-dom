/* const button = document.querySelector<HTMLButtonElement>('button');


button?.addEventListener('click', () => {
   console.log('click');
}); */

/* Counter Exercise
[10:09]
1. Créer les fichier exo-counter.html / .ts
2. Dans le le html, rajouter 2 button, un + et un -
3. Dans le ts, créer une variable counter initialisée à zéro
4. Capturer les 2 button et avec des addEventListener faire que quand on click sur l'un ça incrémente le counter et ça affiche sa valeur en console
5. Faire que quand on click sur l'autre, ça fasse pareil, mais en décrémentant
6. Rajouter un élément span, ou div ou osef dans le html et lui mettre 0 dedans
7. Modifier nos eventListener pour faire qu'à la place ou en plus du console log, ça  aille modifier le innerHTML/textContent de l'élément en question pour lui mettre la valeur actuelle du counter (modifié)
[10:10]
Bonus : Créer un bouton reset. Rajouter une limite au compteur pour qu'il puisse pas aller en dessous de zéro par exemple (avec pourquoi pas le bouton - qui devient non clickable quand c'est le cas) */

let counter:number = 0
const buttonPlus = document.querySelector<HTMLButtonElement>('.plus');
const buttonMoins = document.querySelector<HTMLButtonElement>('.moins');
const div = document.querySelector<HTMLElement>('div');
const buttonReset = document.querySelector<HTMLButtonElement>('.reset')

function counterStrike(){
    if(div){
        div.innerHTML = "Le compteur est à "+counter+ "";
        }
    if (buttonMoins){
        buttonMoins.disabled = counter == 0;
    }
}
counterStrike()

buttonPlus?.addEventListener('click', () => {
    counter += 1;
    console.log(counter);
    counterStrike();
})


buttonMoins?.addEventListener('click', () => {
    if (counter > 0) {
    counter -= 1;
    console.log(counter);
    counterStrike();

    } 
})


buttonReset?.addEventListener('click', () => {
    counter = 0;
    console.log(counter);
    counterStrike();
})



