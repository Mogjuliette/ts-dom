console.log("coucou")

let para2 = document.querySelector<HTMLElement>('#para2')

if(para2){
    para2.style.color = '#0000FF';
    para2.textContent = "modified by JS"
}

let section2 = document.querySelector<HTMLElement>('#section2')

if(section2){
    section2.style.border = '2px black dotted';
}

let s2colorful = document.querySelector<HTMLElement>('#section2 .colorful')

if(s2colorful){
    s2colorful.style.backgroundColor = 'orange';
}

let h2s1 = document.querySelector<HTMLElement>('#section1 h2')

if(h2s1){
    h2s1.style.fontStyle = 'italic';
}

let pcolorful = document.querySelector<HTMLElement>('p .colorful')

if(pcolorful){
    pcolorful.style.display = 'none';
}

let as1 = document.querySelector<HTMLAnchorElement>('#section1 a')

if(as1){
    as1.href = "https://auvergnerhonealpes.simplon.co/lyon.html";
}

let h2s2 = document.querySelector<HTMLElement>('#section2 h2')

if(h2s2){
    h2s2.classList.add("big-text")
}

let p = document.querySelectorAll<HTMLElement>('p')

for (const item of p) {
    item.style.fontStyle = 'italic';
}
    

